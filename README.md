# chatbot_twitch
chatbot twitch built to work with league of legends streamers.
a lot of functionnalities are included.
100% node.js

## Getting started
 To use the bot, you will need node.
 Install the dependancies with
 ```
 npm install
 ```
 "axios": "^1.3.4",
"cheerio": "^1.0.0-rc.12",
"express": "^4.18.2",
"puppeteer": "^19.8.0",
"spotify-web-api-node": "^5.0.2",
"tmi.js": "^1.8.5"

then run 
```
node index.js
```
To make it work you will need some Riotgames and Spotify credential/api keys
 
